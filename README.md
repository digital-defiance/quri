<img src="https://user-images.githubusercontent.com/3766240/174692283-97aa5dee-1bcf-4ff4-a75f-6ce3e2a546af.png" width="192" alt="qURI logo" />

# qURI: Rank anything that can be represented in a URI/URL.
* People
* Transactions
* Web-pages
* Web-sites
* Phone numbers (anti-spam)

Rating strength is affected by your own reputation.
Rankings can then be used by API for reputation systems, anti-spam (email, phone, and web), and more.

## Stack
* Firebase hosting/authentication/database
* npm/node
* Angular
* FontAwesome

## References
- https://www.thisdot.co/blog/realtime-app-with-angular-and-firestore-angularfire
- https://github.com/angular/angularfire/blob/master/docs/auth/getting-started.md
- https://developers.google.com/codelabs/building-a-web-app-with-angular-and-firebase#2
- https://github.com/codediodeio/angular-firestarter
- https://github.com/nibmz7/live-parade-state
- https://github.com/ajboard13/AngularFire2-Authentication-Template/tree/master/src/app
- https://github.com/googlearchive/friendlypix-web
- https://dev.to/jdgamble555/angular-12-with-firebase-9-49a0
- https://modern-admin-docs.web.app/html/ltr/documentation/documentation-setup.html
- https://medium.com/learn-angular/user-handling-with-firebase-and-angular-b188ecb95e2d


<img src="https://user-images.githubusercontent.com/3766240/174692642-94753bcf-31b2-430d-8ad6-a2b67dee48f0.png" width="128" alt="qURI favicon" />
